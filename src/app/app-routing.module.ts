import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './components/login/login.component';
import { TwoComponent } from './components/two/two.component';
import { UnoComponent } from './components/uno/uno.component';
import { AuthGuard } from './guards/auth.guard';
const routes: Routes = [
  
  {path: 'Login', component: LoginComponent },
  {path: 'Uno', component: UnoComponent, canActivate: [AuthGuard] },
  {path: 'Two', component: TwoComponent },
  { path: '', pathMatch: 'full', redirectTo: 'Uno' },
  { path: '**', pathMatch: 'full', redirectTo: 'Uno' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
