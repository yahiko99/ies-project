import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Country } from '../components/models/country.model';

@Injectable({
  providedIn: 'root'
})
export class GeneralService {

  
  constructor(private http: HttpClient) { }

  getCountry():Observable<Country[]>{
    return this.http.get<Country[]>('https://api.first.org/data/v1/countries?region=africa&limit=10&pretty=true')
  }
}
