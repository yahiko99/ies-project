import { Injectable } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';


@Injectable({
  providedIn: 'root'
})
export class SnackbarService {

  constructor(private _snackBar: MatSnackBar) { }

  open(mensaje: string, boton: string): void{
    this._snackBar.open(mensaje, boton, {
      duration: 5 * 1000,
    })
  }
}
