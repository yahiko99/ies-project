import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Login } from '../components/models/login.model';

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  api = environment.api;
  rol: string = '';

  constructor(private http: HttpClient) { }

  setloginData(usuario: Login): Observable<any>{
    console.log(usuario)
    const jsonLogin = JSON.stringify(usuario);
    let headers = new HttpHeaders({
      'Content-Type':'application/json'
    });
    return this.http.post<any>(this.api,jsonLogin,{ headers} );

  }  
  setRol(usuarioTipo: string): void{
    this.rol = usuarioTipo;
    console.log(usuarioTipo)
  }
  getRol(): string{
    return this.rol;
  }
}
