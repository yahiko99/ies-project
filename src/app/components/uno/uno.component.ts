import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { GeneralService } from 'src/app/services/general.service';
import { Country } from '../models/country.model';

@Component({
  selector: 'app-uno',
  templateUrl: './uno.component.html',
  styleUrls: ['./uno.component.css']
})
export class UnoComponent implements OnInit {

   objetKeys = Object.keys;
   array = [ { value: 1 , name: 'CampoUno'}, { value: 2 , name: 'CampoDos'}, { value: 3 , name: 'CampoTres'}, { value: 4 , name: 'CampoCuatro'}, { value: 5 , name: 'CampoCinco'}, { value: 6 , name: 'CampoSeis'}, ];
   resultado : any = [];
   minDate: Date;
   maxDate: Date;
   form: FormGroup;
   //////////////////////////////////Instanciando un nuevo objeto de tipo array country
   paises: Array<Country> = new Array <Country>();
  constructor(private fb: FormBuilder, private serviceG: GeneralService) { 
    this.form = this.createForm(); 

    const currentYear = new Date().getFullYear();
    const currentMonth = new Date().getMonth();
    const currentDay = new Date().getDay();
    this.minDate = new Date(currentYear,  currentMonth - 11, currentDay);
    this.maxDate = new Date(currentYear, currentMonth, currentDay -2);
    console.log(currentYear, currentMonth, this.minDate, this.maxDate)
   }

  ngOnInit(): void {
    console.log(this.array.length)
    this.array.forEach( 
      registro => {
        this.resultado.push([registro.name, registro.value])        
        console.log(this.resultado)      
      }
    )
   
      this.serviceG.getCountry().subscribe((paises: any) =>{
        console.log(paises)
        this.paises = Object.values(paises.data);             
      })
    
  }
  

  exOne(){
    console.log(this.array.length)
  }

  onSubmit() {
    if (this.form.status == 'VALID') {
      console.log(this.form.value)
  
    }
    else{
      console.log("NO es valido")
      alert("FILL ALL FIELDS")
    }
  }


createForm(): FormGroup{
  return new FormGroup(
    {
      nombre: new FormControl('', [Validators.required]), 
      apellidoP: new FormControl('', [Validators.required]),    
      correo: new FormControl('', [Validators.required]),           
      reserva: new FormControl('', [Validators.required]), 
      pais: new FormControl('', [Validators.required]), 
     
    }      

  )
}

}
