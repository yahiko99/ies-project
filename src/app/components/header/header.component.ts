import { Component, OnInit, Input } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  @Input() form: FormGroup;

  constructor() {
    this.form = this.createForm();
   }

  ngOnInit(): void {
  }
  createForm(): FormGroup{
    return new FormGroup(
      {
        nombre: new FormControl('', [Validators.required]), 
        apellidoP: new FormControl('', [Validators.required]),    
        correo: new FormControl('', [Validators.required]),           
        reserva: new FormControl('', [Validators.required]), 
        pais: new FormControl('', [Validators.required]), 
       
      }      
  
    )
  }
}
