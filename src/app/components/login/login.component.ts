import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormControl, FormGroup, FormGroupDirective, NgForm, Validators} from '@angular/forms';
import {ErrorStateMatcher} from '@angular/material/core';
import { Router } from '@angular/router';
import { LoginService } from 'src/app/services/login.service';
import { SnackbarService } from 'src/app/services/snackbar.service';
import { Login } from '../models/login.model';


export class MyErrorStateMatcher implements ErrorStateMatcher {
  isErrorState(control: FormControl | null, form: FormGroupDirective | NgForm | null): boolean {
    const isSubmitted = form && form.submitted;
    return !!(control && control.invalid && (control.dirty || control.touched || isSubmitted));
  }
}

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
 
  form: FormGroup;
  dataLogin: Login = {
    usuario: '', 
    contrasena: ''     
  }
  hide = true;
  

  constructor(private formBuilder: FormBuilder, private loginservice: LoginService, 
    private router: Router, private snackbar: SnackbarService) {
    this.form = this.createForm(); 
   }

  ngOnInit(): void {    
    }

 createForm(): FormGroup{    
    return new FormGroup(
      {
        usuario: new FormControl('', [Validators.required]),    
        contrasena: new FormControl('', [Validators.required]),   
      } 
    )
  }

  onSubmit(event:Event) {
    if (this.form.status == 'VALID') {     
      console.log(this.form.get('usuario')?.value);
      this.dataLogin.usuario = this.form.get('usuario')?.value;
      this.dataLogin.contrasena = this.form.get('contrasena')?.value;
      this.loginservice.setloginData(this.dataLogin).subscribe(
        response => {          
          if (!response) {            
            this.snackbar.open('Ups! Algo salio mal','Ok')
          } else {                    
          if(response.resultado == null){
             this.snackbar.open('Tus credenciales son incorrectas','Ok')
          }else{
              this.loginservice.setRol(response.resultado.desc_rol);
              this.router.navigate(['/Uno']);
            }
          }
        },
        error => {
            this.snackbar.open('Error en la peticion','Ok')
        }

      );
    }
    else{ 
      console.log("NO es valido")
      alert("FILL ALL FIELDS")
    }
  }

  userFormControl = new FormControl('', [
    Validators.required,
    Validators.email,
  ]);

  matcher = new MyErrorStateMatcher();
  
}
